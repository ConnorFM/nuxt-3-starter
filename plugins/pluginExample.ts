import { defineNuxtPlugin } from '#app';

export default defineNuxtPlugin((useNuxtApp) => {
  return {
    provide: {
      hello: () => 'world',
    },
  };
});
