# Nuxt 3 Starter

We recommend to look at the [documentation](https://v3.nuxtjs.org).


Best Nuxt with Plugins:
- [Pinia](https://github.com/posva/pinia)

## Setup

Make sure to install the dependencies

```bash
yarn install
```

## Development

Start the development server on http://localhost:3000

```bash
yarn dev
```

## Production

Build the application for production:

```bash
yarn build
```

```bash
yarn start
```
