import { ref, Ref } from 'vue';
import { defineStore } from 'pinia';

interface CounterState {
  n: number;
  myRef: Ref<string>;
}

export const useCounter = defineStore('counter', {
  state: (): CounterState => ({
    n: 0,
    myRef: ref('Bienvenue'),
  }),
  getters: {
    counterToPrice(state) {
      return `${state.n}€`;
    },
  },
  actions: {
    increment() {
      this.n++;
    },
  },
});
